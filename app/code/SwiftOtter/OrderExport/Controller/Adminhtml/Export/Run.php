<?php

declare(strict_types=1);

namespace SwiftOtter\OrderExport\Controller\Adminhtml\Export;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use SwiftOtter\OrderExport\Model\HeaderDataFactory;

class Run extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'SwiftOtter_OrderExport::OrderExport';
    /**
     * @var JsonFactory
     */
    private $jsonFactory;
    /**
     * @var HeaderDataFactory
     */
    private $headerDataFactory;

    /**
     * Run constructor.
     * @param Action\Context $context
     * @param JsonFactory $jsonFactory
     * @param HeaderDataFactory $headerDataFactory
     */
    public function __construct(
        Action\Context $context,
        JsonFactory $jsonFactory,
        HeaderDataFactory $headerDataFactory
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->headerDataFactory = $headerDataFactory;
    }

    public function execute()
    {
        $headerData = $this->headerDataFactory->create();
        $headerData->setShipDate(new \DateTime($this->getRequest()->getParam('ship_date') ?? ''));
        $headerData->setMerchantNotes((string)$this->getRequest()->getParam('merchant_notes'));
    }
}
