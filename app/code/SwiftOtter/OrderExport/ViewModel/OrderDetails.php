<?php

declare(strict_types=1);

namespace SwiftOtter\OrderExport\ViewModel;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\AuthorizationInterface;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

/**
 * Class OrderDetails
 * @package Magento\Sales\ViewModel\Customer
 */
class OrderDetails implements ArgumentInterface
{
    /**
     * @var FormKey
     */
    private $formKey;
    /**
     * @var UrlInterface
     */
    private $urlBuilder;
    /**
     * @var AuthorizationInterface
     */
    private $authorization;
    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * OrderDetails constructor.
     * @param FormKey $formKey
     * @param UrlInterface $urlBuilder
     * @param AuthorizationInterface $authorization
     * @param RequestInterface $request
     */
    public function __construct(
        FormKey $formKey,
        UrlInterface $urlBuilder,
        AuthorizationInterface $authorization,
        RequestInterface $request
    ) {
        $this->formKey = $formKey;
        $this->urlBuilder = $urlBuilder;
        $this->authorization = $authorization;
        $this->request = $request;
    }

    public function getConfig(): array
    {
        return [
            'sending_message' => __('Sending...'),
            'original_message' => $this->getButtonMessage(),
            'upload_url' => $this->urlBuilder->getUrl(
                'order_export/export/run',
                [
                    'order_id' => (int)$this->request->getParam('order_id')
                ]
            ),
            'form_key' => $this->formKey->getFormKey()
        ];
    }

    public function isAllowed(): bool
    {
        return $this->authorization->isAllowed('SwiftOtter_OrderExport::OrderExport');
    }

    public function getButtonMessage(): string
    {
        return (string)__('Send Order to Fulfillment');
    }
}
