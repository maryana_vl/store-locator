<?php

declare(strict_types=1);

namespace Academy\FirstExtension\Block;

use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;

/**
 * Class RandomProduct
 * @package Academy\FirstExtension\Block
 */

class RandomProduct extends Template
{
    /**
     * @var ProductRepositoryInterface
     */
    private $repository;

    /**
     * RandomProduct constructor.
     * @param ProductRepositoryInterface $productRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        ProductRepositoryInterface $productRepository,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->repository = $productRepository;
    }

    /**
     * @return ProductInterface|null
     */
   public function getProduct() : ?ProductInterface
   {
       $productId = rand(1,1000);
       try {
           $product = $this->repository->getById($productId);
       } catch (NoSuchEntityException $ex) {
           return null;
       }
       return $product;
   }
}
