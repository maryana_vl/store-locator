<?php

namespace Academy\Car\Block\Adminhtml\Car\Edit;

use Magento\Backend\Block\Widget\Context;
use Academy\Car\Api\CarRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var CarRepositoryInterface
     */
    protected $carRepository;

    /**
     * @param Context $context
     * @param CarRepositoryInterface $carRepository
     */
    public function __construct(
        Context $context,
        CarRepositoryInterface $carRepository
    ) {
        $this->context = $context;
        $this->carRepository = $carRepository;
    }

    /**
     * Return Car ID
     *
     * @return int|null
     */
    public function getCarId()
    {
        try {
            return $this->carRepository->getById(
                $this->context->getRequest()->getParam('car_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
