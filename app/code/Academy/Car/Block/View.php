<?php

declare(strict_types=1);

namespace Academy\Car\Block;

use Academy\Car\Api\CarRepositoryInterface;
use Academy\Car\Api\Data\CarInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;

/**
 * Class View
 * @package Academy\Car\Block
 */
class View extends Template
{
    /**
     * @var CarInterface
     */
    private $carModel;

    /**
     * View constructor.
     * @param CarInterface $car
     * @param CarRepositoryInterface $carRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        CarInterface $car,
        CarRepositoryInterface $carRepository,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->carModel = $car;
    }

    /**
     * @param null $id
     * @return CarInterface|null
     */
    public function getCar($id = null) :?CarInterface
    {
        if ($id)
        {
            try {
                /** @var \Academy\Car\Api\Data\CarInterface $car */
                $car = $this->carModel->load($id);
                return $car;
            } catch (NoSuchEntityException $ex) {
                return null;
            }
        }
        return null;
    }

}
