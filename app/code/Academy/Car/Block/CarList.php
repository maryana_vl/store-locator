<?php

declare(strict_types=1);

namespace Academy\Car\Block;

use Academy\Car\Model\Car;
use Academy\Car\Model\ResourceModel\Car\Collection as CarCollection;
use Academy\Car\Model\ResourceModel\Car\CollectionFactory as CollectionFactory;
use Magento\Framework\View\Element\Template;

/**
 * Class CarList
 * @package Academy\Car\Block
 */
class CarList extends Template
{
    /**
     * @var Car
     */
    private $carModel;
    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * View constructor.
     * @param Car $car
     * @param CollectionFactory $collectionFactory
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Car $car,
        CollectionFactory $collectionFactory,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->carModel = $car;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return CarCollection
     */
    public function getCars() : CarCollection
    {
        /** @var CarCollection $collection */

        $collection = $this->collectionFactory->create();

        /*$collection->addFieldToFilter(
            "year_production",
                ["eq" => 2016]
        );*/

        $collection->setOrder("year_production","asc");

        return $collection;
    }

}
