<?php

namespace Academy\Car\Api\Data;

interface CarInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const CAR_ID            = "car_id";
    const BRAND             = "brand";
    const MODEL             = "model";
    const YEAR_PRODUCTION   = "year_production";
    const CREATION_TIME     = "creation_time";
    const UPDATE_TIME       = "update_time";
    /**#@-*/

    /**
     * @return int|null
     */
    public function getId();

    /**
     * @return string
     */
    public function getBrand();

    /**
     * @return string|null
     */
    public function getModel();

    /**
     * @return int|null
     */
    public function getYearProduction();

    /**
     * @return string|null
     */
    public function getCreationTime();

    /**
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * @param int $id
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setId($id);

    /**
     * @param string $brand
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setBrand($brand);

    /**
     *
     * @param string $model
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setModel($model);

    /**
     * @param  int $year
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setYearProduction($year);

    /**
     * @param string $time
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setCreationTime($time);

    /**
     * @param string $time
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setUpdateTime($time);
}
