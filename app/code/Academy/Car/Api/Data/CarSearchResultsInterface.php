<?php

namespace Academy\Car\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface CarSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get cars list.
     *
     * @return \Academy\Car\Api\Data\CarInterface[]
     */
    public function getItems();

    /**
     * Set cars list.
     *
     * @param \Academy\Car\Api\Data\CarInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

