<?php

namespace Academy\Car\Api;

interface CarRepositoryInterface
{
    /**
     * Save car.
     *
     * @param \Academy\Car\Api\Data\CarInterface $car
     * @return \Academy\Car\Api\Data\CarInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\CarInterface $car);

    /**
     * Retrieve car.
     *
     * @param int $carId
     * @return \Academy\Car\Api\Data\CarInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($carId);

    /**
     * Retrieve cars matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Academy\Car\Api\Data\CarSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete car.
     *
     * @param \Academy\Car\Api\Data\CarInterface $car
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\CarInterface $car);

    /**
     * Delete car by ID.
     *
     * @param int $carId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($carId);
}
