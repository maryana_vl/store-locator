<?php

declare(strict_types=1);

namespace Academy\Car\Model;

use Academy\Car\Api\Data\CarSearchResultsInterface;
use Magento\Framework\Api\SearchResults;


class CarSearchResults extends SearchResults implements CarSearchResultsInterface
{
}
