<?php

namespace Academy\Car\Model\ResourceModel\Car;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Car
 * @package Academy\Car\Model\ResourceModel\Collection
 */
class Collection extends AbstractCollection
{
    public function _construct() //phpcs:ignore Magento2.CodeAnalysis.EmptyBlock
    {
        parent::_init(
            \Academy\Car\Model\Car::class,
            \Academy\Car\Model\ResourceModel\Car::class
        );
    }
}
