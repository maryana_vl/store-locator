<?php

namespace Academy\Car\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Car
 * @package Academy\Car\Model\ResourceModel
 */
class Car extends AbstractDb
{
    public function _construct()
    {
        parent::_init("elogic_car","car_id");
    }
}
