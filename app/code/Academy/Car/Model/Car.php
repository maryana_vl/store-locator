<?php

namespace Academy\Car\Model;

use Academy\Car\Api\Data\CarInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Car
 * @package Academy\Car\Model
 */
class Car extends AbstractModel implements CarInterface
{
    public function _construct()
    {
        $this->_init(\Academy\Car\Model\ResourceModel\Car::class);
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return parent::getData(self::CAR_ID);
    }

    /**
     * @return string
     */
    public function getBrand()
    {
        return parent::getData(self::BRAND);
    }

    /**
     * @return string|null
     */
    public function getModel()
    {
        return parent::getData(self::MODEL);
    }

    /**
     * @return int|null
     */
    public function getYearProduction()
    {
        return parent::getData(self::YEAR_PRODUCTION);
    }

    /**
     * @return string|null
     */
    public function getCreationTime()
    {
        return parent::getData(self::CREATION_TIME);
    }

    /**
     * @return string|null
     */
    public function getUpdateTime()
    {
        return parent::getData(self::UPDATE_TIME);
    }

    /**
     * @param int $id
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setId($id)
    {
        return $this->setData(self::CAR_ID, $id);
    }

    /**
     * @param string $brand
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setBrand($brand)
    {
        return $this->setData(self::BRAND, $brand);
    }

    /**
     *
     * @param string $model
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setModel($model)
    {
        return $this->setData(self::MODEL, $model);
    }

    /**
     * @param  int $year
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setYearProduction($year)
    {
        return $this->setData(self::YEAR_PRODUCTION, $year);
    }

    /**
     * @param string $time
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setCreationTime($time)
    {
        return $this->setData(self::CREATION_TIME, $time);
    }

    /**
     * @param string $time
     * @return \Academy\Car\Api\Data\CarInterface
     */
    public function setUpdateTime($time)
    {
        return $this->setData(self::UPDATE_TIME, $time);
    }
}
