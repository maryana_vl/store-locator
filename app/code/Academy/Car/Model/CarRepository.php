<?php

namespace Academy\Car\Model;

use Academy\Car\Api\CarRepositoryInterface;
use Academy\Car\Api\Data;
use Academy\Car\Model\ResourceModel\Car\CollectionFactory as CarCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class CarRepository implements CarRepositoryInterface
{
    /**
     * @var ResourceModel\Car
     */
    private $resource;
    /**
     * @var Data\CarInterfaceFactory
     */
    private $carFactory;
    /**
     * @var CarCollectionFactory
     */
    private $carCollectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var Data\CarSearchResultsInterface
     */
    private $searchResultsFactory;

    /**
     * CarRepository constructor.
     * @param ResourceModel\Car $resource
     * @param Data\CarInterfaceFactory $dataCarFactory
     * @param CarCollectionFactory $carCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param Data\CarSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Academy\Car\Model\ResourceModel\Car $resource,
        Data\CarInterfaceFactory $dataCarFactory,
        CarCollectionFactory $carCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        Data\CarSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->resource = $resource;
        $this->carFactory = $dataCarFactory;
        $this->carCollectionFactory = $carCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @param Data\CarInterface $car
     * @return Data\CarInterface
     * @throws CouldNotSaveException
     */
    public function save(\Academy\Car\Api\Data\CarInterface $car)
    {
        try {
            $this->resource->save($car);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $car;
    }

    /**
     * @param int $carId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($carId)
    {
        $car = $this->carFactory->create();
        $this->resource->load($car, $carId);
        if (!$car->getId()) {
            throw new NoSuchEntityException(__('The car with the "%1" ID doesn\'t exist.', $carId));
        }
        return $car;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Data\BlockSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Block\Collection $collection */
        $collection = $this->carCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var Data\BlockSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @param Data\CarInterface $car
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Academy\Car\Api\Data\CarInterface $car)
    {
        try {
            $this->resource->delete($car);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param int $carId
     * @return bool
     */
    public function deleteById($carId)
    {
        return $this->delete($this->getById($carId));
    }
}
