<?php

namespace Academy\Car\Controller\Adminhtml\Car;


use Academy\Car\Api\CarRepositoryInterface;
use Academy\Car\Model\Car;
use Academy\Car\Model\CarFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Academy_Car::car';

    /**
     * @var PageFactory
     */
    private $pageFactory;
    /**
     * @var CarRepositoryInterface
     */
    private $carRepository;
    /**
     * @var CarFactory
     */
    private $carFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param CarRepositoryInterface $carRepository
     * @param CarFactory $carFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        CarRepositoryInterface $carRepository,
        CarFactory $carFactory
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->carRepository = $carRepository;
        $this->carFactory = $carFactory;
    }

    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();

        $data = $this->getRequest()->getPostValue();

        if (empty($data['car_id'])) {
            $data['car_id'] = null;
        }

        /** @var Car $car */
        $car = $this->carFactory->create();

        $car->setData($data);

        $this->carRepository->save($car);

        return $redirect->setPath("*/*/");
    }
}
