<?php

namespace Academy\Car\Controller\Adminhtml\Car;

use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Academy_Car::car';

    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
    }

    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('car_id');
        $model = $this->_objectManager->create(\Academy\Car\Model\Car::class);

        // 2. Initial checking
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This car no longer exists.'));
                $redirect = $this->resultRedirectFactory->create();
                return $redirect->setPath('*/*/');
            }
        }

        // 5. Build edit form
        $resultPage = $this->pageFactory->create();

        $resultPage->getConfig()->getTitle()->prepend(__('Cars'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getTitle() : __('New Car'));
        return $resultPage;
    }
}
