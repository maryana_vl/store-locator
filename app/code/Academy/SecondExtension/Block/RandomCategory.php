<?php

declare(strict_types=1);

namespace Academy\SecondExtension\Block;

use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;

/**
 * Class RandomCategory
 * @package Academy\SecondExtension\Block
 */

class RandomCategory extends Template
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $repository;

    /**
     * RandomCategory constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->repository = $categoryRepository;
    }

    /**
     * @return CategoryInterface|null
     */
    public function getCategory() : ?CategoryInterface
    {
        $categoryId = rand(1,20);
        try {
            $category = $this->repository->get($categoryId);
        } catch (NoSuchEntityException $ex) {
            return null;
        }
        return $category;
    }
}
