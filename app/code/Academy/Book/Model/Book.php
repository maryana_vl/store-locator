<?php

namespace Academy\Book\Model;

use Magento\Framework\Model\AbstractModel;
use Academy\Book\Api\Data\BookInterface;

/**
 * Class Book
 * @package Academy\Book\Model
 */
class Book extends AbstractModel implements BookInterface
{
    protected $_eventPrefix = 'academy_book';
    protected $_eventObject = 'book';

    public function _construct()
    {
        $this->_init(\Academy\Book\Model\ResourceModel\Book::class);
    }

    /**
     * @return int|null
     */
    public function getId()
    {
        return parent::getData(self::BOOK_ID);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return parent::getData(self::TITLE);
    }

    /**
     * @return string|null
     */
    public function getAuthor()
    {
        return parent::getData(self::AUTHOR);
    }

    /**
     * @return int|null
     */
    public function getYearPublished()
    {
        return parent::getData(self::YEAR_PUBLISHED);
    }

    /**
     * @return string|null
     */
    public function getCreationTime()
    {
        return parent::getData(self::CREATION_TIME);
    }

    /**
     * @return string|null
     */
    public function getUpdateTime()
    {
        return parent::getData(self::UPDATE_TIME);
    }

    /**
     * @param int $id
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setId($id)
    {
        return $this->setData(self::BOOK_ID, $id);
    }

    /**
     * @param string $title
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setTitle($title)
    {
        return $this->setData(self::TITLE, $title);
    }

    /**
     *
     * @param string $author
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setAuthor($author)
    {
        return $this->setData(self::AUTHOR, $author);
    }

    /**
     * @param  int $year
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setYearPublished($year)
    {
        return $this->setData(self::YEAR_PUBLISHED, $year);
    }

    /**
     * @param string $time
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setCreationTime($time)
    {
        return $this->setData(self::CREATION_TIME, $time);
    }

    /**
     * @param string $time
     * @return \Academy\Book\Api\Data\BookInterface
     */
    public function setUpdateTime($time)
    {
        return $this->setData(self::UPDATE_TIME, $time);
    }
}