<?php

namespace Academy\Book\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Book
 * @package Academy\Book\Model
 */
class Book extends AbstractDb
{
    public function _construct()
    {
        parent::_init("elogic_book","book_id");
    }
}