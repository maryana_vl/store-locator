<?php

namespace Academy\Book\Model;

use Academy\Book\Api\Data;
use Academy\Book\Api\BookRepositoryInterface;
use Academy\Book\Model\ResourceModel\Book\CollectionFactory as BookCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;

class BookRepository implements BookRepositoryInterface
{
    /**
     * @var ResourceModel\Book
     */
    private $resource;
    /**
     * @var Data\BookInterfaceFactory
     */
    private $bookFactory;
    /**
     * @var BookCollectionFactory
     */
    private $bookCollectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var Data\BookSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;

    /**
     * BookRepository constructor.
     * @param ResourceModel\Book $resource
     * @param Data\BookInterfaceFactory $dataBookFactory
     * @param BookCollectionFactory $bookCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param Data\BookSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Academy\Book\Model\ResourceModel\Book $resource,
        \Academy\Book\Api\Data\BookInterfaceFactory $dataBookFactory,
        BookCollectionFactory $bookCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        Data\BookSearchResultsInterfaceFactory $searchResultsFactory
    )
    {

        $this->resource = $resource;
        $this->bookFactory = $dataBookFactory;
        $this->bookCollectionFactory = $bookCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @param Data\BookInterface $book
     * @return Data\BookInterface
     * @throws CouldNotSaveException
     */
    public function save(\Academy\Book\Api\Data\BookInterface $book)
    {
        try {
            $this->resource->save($book);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $book;
    }

    /**
     * @param int $bookId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($bookId)
    {
        $book = $this->bookFactory->create();
        $this->resource->load($book, $bookId);
        if (!$book->getId()) {
            throw new NoSuchEntityException(__('The book with the "%1" ID doesn\'t exist.', $bookId));
        }
        return $book;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Data\BlockSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Magento\Cms\Model\ResourceModel\Block\Collection $collection */
        $collection = $this->bookCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var Data\BlockSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @param Data\BookInterface $book
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Academy\Book\Api\Data\BookInterface $book)
    {
        try {
            $this->resource->delete($book);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param int $bookId
     * @return bool
     */
    public function deleteById($bookId)
    {
        return $this->delete($this->getById($bookId));
    }
}
