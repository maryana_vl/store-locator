<?php

declare(strict_types=1);

namespace Academy\Book\Block;

use Academy\Book\Api\BookRepositoryInterface;
use Academy\Book\Api\Data\BookInterface;
use Academy\Book\Model\Book;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;

/**
 * Class RandomProduct
 * @package Academy\Book\Block
 */
class View extends Template
{
    /**
     * @var BookRepositoryInterface
     */
    protected $bookRepository;

    /**
     * View constructor.
     * @param BookRepositoryInterface $bookRepository
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        BookRepositoryInterface $bookRepository,
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->bookRepository = $bookRepository;
    }

    /**
     * @param null $id
     * @return BookInterface|null
     */
    public function getBook($id = null) :?BookInterface
    {
        if ($id)
        {
            try {
                /** @var \Academy\Book\Model\Book $book */
                $book = $this->bookRepository->getById($id);
                return $book;
            } catch (NoSuchEntityException $ex) {
                return null;
            }
        }
        return null;
    }

}