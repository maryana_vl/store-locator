<?php

namespace Academy\Book\Controller\Adminhtml\Book;


use Academy\Book\Api\BookRepositoryInterface;
use Academy\Book\Model\Book;
use Academy\Book\Model\BookFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Save extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Academy_Book::book';

    /**
     * @var PageFactory
     */
    private $pageFactory;
    /**
     * @var BookRepositoryInterface
     */
    private $bookRepository;
    /**
     * @var BookFactory
     */
    private $bookFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param BookRepositoryInterface $bookRepository
     * @param BookFactory $bookFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        BookRepositoryInterface $bookRepository,
        BookFactory $bookFactory
    )
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->bookRepository = $bookRepository;
        $this->bookFactory = $bookFactory;
    }

    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();

        $data = $this->getRequest()->getPostValue();

        if (empty($data['book_id'])) {
            $data['book_id'] = null;
        }

        /** @var Book $book */
        $book = $this->bookFactory->create();

        $book->setData($data);

        $this->bookRepository->save($book);

        return $redirect->setPath("*/*/");
    }
}