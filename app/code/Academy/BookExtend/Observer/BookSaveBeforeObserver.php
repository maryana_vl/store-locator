<?php

namespace Academy\BookExtend\Observer;

use Academy\Book\Api\Data\BookInterface;

class BookSaveBeforeObserver implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var BookInterface $book */
        $book = $observer->getBook();

        if ($book->getYearPublished()==0)
        {
            $book->setYearPublished(2000);
        }
    }
}
