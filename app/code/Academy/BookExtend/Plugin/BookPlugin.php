<?php

namespace Academy\BookExtend\Plugin;

use Academy\Book\Model\Book;

class BookPlugin {

    public function beforeSetYearPublished(Book $subject, $year)
    {
//        var_dump($year);die;
        if ((int)$year == 0)
        {
            return ["1200"];
        }
    }

    public function afterGetAuthor(Book $subject, $result)
    {
//        var_dump($result);die;
        if (empty($result))
        {
            return "no author";
        }
    }

}
