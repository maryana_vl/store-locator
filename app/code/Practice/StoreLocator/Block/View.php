<?php

declare(strict_types=1);

namespace Practice\StoreLocator\Block;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Practice\StoreLocator\Api\StoreLocatorRepositoryInterface;
use Practice\StoreLocator\Helper\Image as ImageHelper;
use Magento\Framework\UrlInterface;

/**
 * Class View
 * @package Practice\StoreLocator\Block
 */
class View extends Template
{
    /**
     * @var StoreLocatorRepositoryInterface
     */
    private $storeLocatorRepository;
    /**
     * @var RequestInterface
     */
    protected $request;
    /**
     * @var ImageHelper
     */
    protected $imageHelper;
    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * View constructor.
     * @param StoreLocatorRepositoryInterface $storeLocatorRepository
     * @param Template\Context $context
     * @param RequestInterface $request
     * @param ImageHelper $imageHelper
     * @param UrlInterface $url
     */
    public function __construct(
        StoreLocatorRepositoryInterface $storeLocatorRepository,
        Template\Context $context,
        RequestInterface $request,
        ImageHelper $imageHelper,
        UrlInterface $url,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->storeLocatorRepository = $storeLocatorRepository;
        $this->request = $request;
        $this->imageHelper = $imageHelper;
        $this->url = $url;
    }

    /**
     * @return \Practice\StoreLocator\Model\StoreLocator|null
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getStore()
    {
        $id = $this->request->getParam('id');
        try {
            /** @var \Practice\StoreLocator\Model\StoreLocator $store */
            $store = $this->storeLocatorRepository->getById($id);
            return $store;
        } catch (NoSuchEntityException $ex) {
            return null;
        }
    }

    public function getStoreImageUrl($store)
    {
        return $this->imageHelper->getImageUrl($store->getImage());
    }

        /**
     * @param $store_id
     * @return string
     */
    public function goBack()
    {
        return $this->url->getUrl('storelocator');
    }
}
