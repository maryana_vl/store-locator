<?php

declare(strict_types=1);

namespace Practice\StoreLocator\Block;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Practice\StoreLocator\Helper\Data as StoreLocatorHelper;
use Practice\StoreLocator\Helper\Image as ImageHelper;
use Practice\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory as CollectionFactory;
use Practice\StoreLocator\Model\StoreLocator;

/**
 * Class StoreList
 * @package Practice\StoreLocator\Block
 */
class StoreList extends Template
{
    /**
     * @var StoreLocator
     */
    protected $storeLocator;
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    /**
     * @var StoreLocatorHelper
     */
    protected $storeLocatorHelper;
    /**
     * @var ImageHelper
     */
    protected $imageHelper;
    /**
     * @var UrlInterface
     */
    protected $url;

    /**
     * View constructor.
     * @param StoreLocator $storeLocator
     * @param CollectionFactory $collectionFactory
     * @param StoreLocatorHelper $storeLocatorHelper
     * @param ImageHelper $imageHelper
     * @param UrlInterface $url
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        StoreLocator $storeLocator,
        CollectionFactory $collectionFactory,
        StoreLocatorHelper $storeLocatorHelper,
        ImageHelper $imageHelper,
        UrlInterface $url,
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->storeLocator = $storeLocator;
        $this->collectionFactory = $collectionFactory;
        $this->storeLocatorHelper = $storeLocatorHelper;
        $this->imageHelper = $imageHelper;
        $this->url = $url;
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if ($this->getStores()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'custom.history.pager'
            )->setAvailableLimit([5 => 5, 10 => 10, 15 => 15, 20 => 20])
                ->setShowPerPage(true)->setCollection(
                    $this->getStores()
                );
            $this->setChild('pager', $pager);
            $this->getStores()->load();
        }
        return $this;
    }
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getStores()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : 5;

        $collection = $this->collectionFactory->create();

        $collection->setOrder("store_name", "asc");
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);

        return $collection;
    }

    public function getStoreImageUrl($store)
    {
        return $this->imageHelper->getImageUrl($store->getImage());
    }

//    /**
//     * @param $store_id
//     * @return string
//     */
//    public function getStoreUrl($store_id)
//    {
//        return $this->url->getUrl('storelocator/store/view', ['id' => $store_id]);
//    }

    /**
     * @param $store_id
     * @return string
     */
    public function getStoreUrl($urlKey)
    {
        return $this->url->getUrl('stores/' . $urlKey . '.html');
    }
}
