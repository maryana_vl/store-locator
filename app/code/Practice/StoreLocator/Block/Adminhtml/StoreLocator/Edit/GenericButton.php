<?php

namespace Practice\StoreLocator\Block\Adminhtml\StoreLocator\Edit;

use Magento\Backend\Block\Widget\Context;
use Practice\StoreLocator\Api\StoreLocatorRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var StoreLocatorRepositoryInterface
     */
    protected $storeRepository;

    /**
     * @param Context $context
     * @param StoreLocatorRepositoryInterface $storeRepository
     */
    public function __construct(
        Context $context,
        StoreLocatorRepositoryInterface $storeRepository
    ) {
        $this->context = $context;
        $this->storeRepository = $storeRepository;
    }

    /**
     * Return Store ID
     *
     * @return int|null
     */
    public function getStoreId()
    {
        try {
            return $this->storeRepository->getById(
                $this->context->getRequest()->getParam('store_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
