<?php

namespace Practice\StoreLocator\Helper;

use Practice\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory as StoreLocatorCollectionFactory;

class UrlKey extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Data
     */
    protected $dataHelper;
    /**
     * @var StoreLocatorCollectionFactory
     */
    protected $collectionFactory;

    /**
     * LatLongCoordinates constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param Data $dataHelper
     * @param StoreLocatorCollectionFactory $collectionFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Data $dataHelper,
        StoreLocatorCollectionFactory $collectionFactory
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
        $this->collectionFactory = $collectionFactory;
    }

    public function getStoreUrlKey($storeName)
    {
        $urlKey = trim($storeName);
        $urlKey = preg_replace('#[^0-9a-z]+#i', '-', $urlKey);
        $urlKey = strtolower($urlKey);

        $newUrlKey = null;
        $counter = 1;

        $collection = $this->collectionFactory->create();
        $urlKeyArray = $collection->getColumnValues('url_key');

        if (!in_array($urlKey, $urlKeyArray)) {
            $newUrlKey = $urlKey;
        } else {
            while (in_array($urlKey . $counter, $urlKeyArray)) {
                $counter++;
            }
            $newUrlKey = $urlKey . $counter;
        }
        return $newUrlKey;
    }
}
