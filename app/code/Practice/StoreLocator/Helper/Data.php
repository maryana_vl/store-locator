<?php

namespace Practice\StoreLocator\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Data
{
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    const XML_PATH_IS_ENABLED = "storelocator/general/enabled";
    const XML_PATH_GOOGLE_MAPS_API = "storelocator/map_settings/api_key";

    /**
     * Data constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    public function isEnabled()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_IS_ENABLED);
    }

    public function getApiKey()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_GOOGLE_MAPS_API);
    }
}
