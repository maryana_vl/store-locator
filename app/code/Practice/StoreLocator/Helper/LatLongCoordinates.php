<?php

namespace Practice\StoreLocator\Helper;

class LatLongCoordinates extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Data
     */
    protected $dataHelper;

    /**
     * LatLongCoordinates constructor.
     * @param \Magento\Framework\App\Helper\Context $context
     * @param Data $dataHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        Data $dataHelper
    ) {
        parent::__construct($context);
        $this->dataHelper = $dataHelper;
    }

    public function getLatLong($address)
    {
        if (!empty($address)) {
            //Formatted address
            $formattedAddr = urlencode($address);
            //Send request and receive json data by address
            $geocodeFromAddr = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address={$formattedAddr}&key={$this->dataHelper->getApiKey()}");
            $output = json_decode($geocodeFromAddr,true);

            if ($output['status'] == 'OK') {
                //Get latitude and longitute from json data
                $data = [];
                $data['latitude'] = isset($output['results'][0]['geometry']['location']['lat']) ? $output['results'][0]['geometry']['location']['lat'] : "";
                $data['longitude'] = isset($output['results'][0]['geometry']['location']['lng']) ? $output['results'][0]['geometry']['location']['lng'] : "";
                //Return latitude and longitude of the given address
                if (!empty($data)) {
                    return $data;
                } else {
                    return false;
                }
            } else {
                $this->_logger->error($output['status']);
                return false;
            }
        }
    }
}
