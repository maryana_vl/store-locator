<?php

namespace Practice\StoreLocator\Model;

use Magento\Framework\Model\AbstractModel;
use Practice\StoreLocator\Api\Data\StoreLocatorInterface;

/**
 * Class StoreLocator
 * @package Practice\StoreLocator\Model
 */
class StoreLocator extends AbstractModel implements StoreLocatorInterface
{
    protected $_eventPrefix = 'practice_storelocator';
    protected $_eventObject = 'storelocator';

    public function _construct()
    {
        $this->_init(\Practice\StoreLocator\Model\ResourceModel\StoreLocator::class);
    }

    /**
     * Retrieve store ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::STORE_ID);
    }

    /**
     * Retrieve store name
     *
     * @return string
     */
    public function getStoreName()
    {
        return parent::getData(self::STORE_NAME);
    }

    /**
     * Retrieve store description
     *
     * @return string
     */
    public function getDescription()
    {
        return parent::getData(self::DESCRIPTION);
    }

    /**
     * Retrieve store image
     *
     * @return string
     */
    public function getImage()
    {
        return parent::getData(self::IMAGE);
    }

    /**
     * Retrieve store country
     *
     * @return string
     */
    public function getCountry()
    {
        return parent::getData(self::COUNTRY);
    }

    /**
     * Retrieve store city
     *
     * @return string
     */
    public function getCity()
    {
        return parent::getData(self::CITY);
    }

    /**
     * Retrieve store address
     *
     * @return string
     */
    public function getAddress()
    {
        return parent::getData(self::ADDRESS);
    }

    /**
     * Retrieve store work schedule
     *
     * @return string
     */
    public function getWorkSchedule()
    {
        return parent::getData(self::WORK_SCHEDULE);
    }

    /**
     * Retrieve store longitude
     *
     * @return float
     */
    public function getLongitude()
    {
        return parent::getData(self::LONGITUDE);
    }

    /**
     * Retrieve store latitude
     *
     * @return float
     */
    public function getLatitude()
    {
        return parent::getData(self::LATITUDE);
    }

    /**
     * Retrieve store url key
     *
     * @return string
     */
    public function getUrlKey()
    {
        return parent::getData(self::URL_KEY);
    }

    /**
     * Retrieve store creation time
     *
     * @return string
     */
    public function getCreationTime()
    {
        return parent::getData(self::CREATION_TIME);
    }

    /**
     * Retrieve store update time
     *
     * @return string
     */
    public function getUpdateTime()
    {
        return parent::getData(self::UPDATE_TIME);
    }


    /**
     * Set ID
     *
     * @param int $id
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setId($id)
    {
        return $this->setData(self::STORE_ID, $id);
    }

    /**
     * Set store name
     *
     * @param string $name
     * @return StoreLocatorInterface
     */
    public function setStoreName($name)
    {
        return $this->setData(self::STORE_NAME, $name);
    }

    /**
     * Set description
     *
     * @param string $description
     * @return StoreLocatorInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Set image
     *
     * @param string $image
     * @return StoreLocatorInterface
     */
    public function setImage($image)
    {
        return $this->setData(self::IMAGE, $image);
    }

    /**
     * Set country
     *
     * @param string $country
     * @return StoreLocatorInterface
     */
    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }

    /**
     * Set city
     *
     * @param string $city
     * @return StoreLocatorInterface
     */
    public function setCity($city)
    {
        return $this->setData(self::CITY, $city);
    }

    /**
     * Set address
     *
     * @param string $address
     * @return StoreLocatorInterface
     */
    public function setAddress($address)
    {
        return $this->setData(self::ADDRESS, $address);
    }

    /**
     * Set work schedule
     *
     * @param string $workSchedule
     * @return StoreLocatorInterface
     */
    public function setWorkSchedule($workSchedule)
    {
        return $this->setData(self::WORK_SCHEDULE, $workSchedule);
    }

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return StoreLocatorInterface
     */
    public function setLongitude($longitude)
    {
        return $this->setData(self::LONGITUDE, $longitude);
    }

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return StoreLocatorInterface
     */
    public function setLatitude($latitude)
    {
        return $this->setData(self::LATITUDE, $latitude);
    }

    /**
     * Set store url key
     *
     * @param string $urlKey
     * @return StoreLocatorInterface
     */
    public function setUrlKey($urlKey)
    {
        return $this->setData(self::URL_KEY, $urlKey);
    }

    /**
     * Set creation time
     *
     * @param string $time
     * @return StoreLocatorInterface
     */
    public function setCreationTime($time)
    {
        return $this->setData(self::CREATION_TIME, $time);
    }

    /**
     * Set update time
     *
     * @param string $time
     * @return StoreLocatorInterface
     */
    public function setUpdateTime($time)
    {
        return $this->setData(self::UPDATE_TIME, $time);
    }
}
