<?php

declare(strict_types=1);

namespace Practice\StoreLocator\Model;

use Practice\StoreLocator\Api\Data\StoreLocatorSearchResultsInterface;
use Magento\Framework\Api\SearchResults;


class StoreLocatorSearchResults extends SearchResults implements StoreLocatorSearchResultsInterface
{
}
