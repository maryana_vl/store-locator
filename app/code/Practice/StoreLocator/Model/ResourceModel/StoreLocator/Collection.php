<?php

namespace Practice\StoreLocator\Model\ResourceModel\StoreLocator;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Practice\StoreLocator\Model\ResourceModel\StoreLocator
 */
class Collection extends AbstractCollection
{
    protected $_idFieldName = 'store_id';

    public function _construct() //phpcs:ignore Magento2.CodeAnalysis.EmptyBlock
    {
        parent::_init(
            \Practice\StoreLocator\Model\StoreLocator::class,
            \Practice\StoreLocator\Model\ResourceModel\StoreLocator::class
        );
    }
}
