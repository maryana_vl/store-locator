<?php

namespace Practice\StoreLocator\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class StoreLocator
 * @package Practice\StoreLocator\Model\ResourceModel
 */
class StoreLocator extends AbstractDb
{
    public function _construct()
    {
        parent::_init("store_locator","store_id");
    }
}
