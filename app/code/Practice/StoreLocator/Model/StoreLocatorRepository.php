<?php

namespace Practice\StoreLocator\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Practice\StoreLocator\Api\Data;
use Practice\StoreLocator\Api\StoreLocatorRepositoryInterface;
use Practice\StoreLocator\Model\ResourceModel\StoreLocator\CollectionFactory as StoreLocatorCollectionFactory;

class StoreLocatorRepository implements StoreLocatorRepositoryInterface
{
    /**
     * @var ResourceModel\StoreLocator
     */
    private $resource;
    /**
     * @var StoreLocatorCollectionFactory
     */
    private $storeCollectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;
    /**
     * @var Data\StoreLocatorSearchResultsInterfaceFactory
     */
    private $searchResultsFactory;
    /**
     * @var Data\StoreLocatorInterfaceFactory
     */
    private $dataStoreFactory;

    /**
     * BookRepository constructor.
     * @param ResourceModel\StoreLocator $resource
     * @param Data\StoreLocatorInterfaceFactory $dataStoreFactory
     * @param StoreLocatorCollectionFactory $storeCollectionFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param Data\StoreLocatorSearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Practice\StoreLocator\Model\ResourceModel\StoreLocator $resource,
        \Practice\StoreLocator\Api\Data\StoreLocatorInterfaceFactory $dataStoreFactory,
        StoreLocatorCollectionFactory $storeCollectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        Data\StoreLocatorSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->resource = $resource;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataStoreFactory = $dataStoreFactory;
        $this->storeCollectionFactory = $storeCollectionFactory;
    }

    /**
     * @param Data\StoreLocatorInterface $store
     * @return Data\StoreLocatorInterface
     * @throws CouldNotSaveException
     */
    public function save(\Practice\StoreLocator\Api\Data\StoreLocatorInterface $store)
    {
        try {
            $this->resource->save($store);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $store;
    }

    /**
     * @param int $storeId
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($storeId)
    {
        $store = $this->dataStoreFactory->create();
        $this->resource->load($store, $storeId);
        if (!$store->getId()) {
            throw new NoSuchEntityException(__('The store with the "%1" ID doesn\'t exist.', $storeId));
        }
        return $store;
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return Data\StoreLocatorSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Practice\StoreLocator\Model\ResourceModel\StoreLocator\Collection $collection */
        $collection = $this->storeCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var Data\StoreLocatorSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @param Data\StoreLocatorInterface $store
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Practice\StoreLocator\Api\Data\StoreLocatorInterface $store)
    {
        try {
            $this->resource->delete($store);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param int $storeId
     * @return bool
     */
    public function deleteById($storeId)
    {
        return $this->delete($this->getById($storeId));
    }
}
