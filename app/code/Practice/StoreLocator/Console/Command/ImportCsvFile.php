<?php
namespace Practice\StoreLocator\Console\Command;

use Practice\StoreLocator\Model\StoreLocatorFactory;
use Practice\StoreLocator\Model\StoreLocatorRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class SomeCommand
 */
class ImportCsvFile extends Command
{
    const NAME = 'path';

    protected $csv;
    /**
     * @var StoreLocatorFactory
     */
    protected $storeLocatorFactory;
    /**
     * @var StoreLocatorRepository
     */
    protected $storeLocatorRepository;

    /**
     * ImportCsvFile constructor.
     * @param string|null $name
     * @param \Magento\Framework\File\Csv $csv
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param StoreLocatorRepository $storeLocatorRepository
     */
    public function __construct(
        string $name = null,
        \Magento\Framework\File\Csv $csv,
        StoreLocatorFactory $storeLocatorFactory,
        StoreLocatorRepository $storeLocatorRepository
    ) {
        parent::__construct($name);
        $this->csv = $csv;
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->storeLocatorRepository = $storeLocatorRepository;
    }

    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setDescription('Imports stores from CSV file');
        $this->addOption(
            self::NAME,
            null,
            InputOption::VALUE_REQUIRED,
            'Import CSV'
        );

        parent::configure();
    }

    /**
     * Execute the command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            if ($csv = $input->getOption(self::NAME)) {
                $output->writeln('<info>Import from `' . $csv . '`...</info>');
                $data = $this->csv->getData($csv);

                $headersRow = array_shift($data);
                $name = array_search('name', $headersRow);
                $description = array_search('description', $headersRow);
                $country = array_search('country', $headersRow);
                $city = array_search('city', $headersRow);
                $address = array_search('address', $headersRow);
                $schedule = array_search('schedule', $headersRow);

                foreach ($data as $value) {
                    $store = $this->storeLocatorFactory->create()
                        ->setStoreName($value[$name])
                        ->setDescription($value[$description])
                        ->setCountry($value[$country])
                        ->setCity($value[$city])
                        ->setAddress($value[$address])
                        ->setWorkSchedule($value[$schedule]);

                    $this->storeLocatorRepository->save($store);
                }
                $output->writeln('<info>Import done successfully.</info>');
            }
        } catch (\Exception $e) {
            $output->writeln('An Error occurred.');
            $output->writeln($e->getMessage());
        }
    }
}
