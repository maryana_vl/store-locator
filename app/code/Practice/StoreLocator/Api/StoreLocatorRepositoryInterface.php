<?php

namespace Practice\StoreLocator\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * CMS page CRUD interface.
 * @api
 * @since 100.0.2
 */
interface StoreLocatorRepositoryInterface
{
    /**
     * Save store.
     *
     * @param \Practice\StoreLocator\Api\Data\StoreLocatorInterface $store
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Practice\StoreLocator\Api\Data\StoreLocatorInterface $store);

    /**
     * Retrieve store.
     *
     * @param int $storeId
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storeId);

    /**
     * Retrieve stores matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete store.
     *
     * @param \Practice\StoreLocator\Api\Data\StoreLocatorInterface $store
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Practice\StoreLocator\Api\Data\StoreLocatorInterface $store);

    /**
     * Delete store by ID.
     *
     * @param int $storeId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($storeId);
}
