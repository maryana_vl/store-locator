<?php

namespace Practice\StoreLocator\Api\Data;

interface StoreLocatorInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const STORE_ID          = "store_id";
    const STORE_NAME        = "store_name";
    const DESCRIPTION       = "description";
    const IMAGE             = "image";
    const COUNTRY           = "country";
    const CITY              = "city";
    const ADDRESS           = "address";
    const WORK_SCHEDULE     = "work_schedule";
    const LONGITUDE         = "longitude";
    const LATITUDE          = "latitude";
    const URL_KEY           = "url_key";
    const CREATION_TIME     = "creation_time";
    const UPDATE_TIME       = "update_time";

    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get store name
     *
     * @return string
     */
    public function getStoreName();

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * Get image
     *
     * @return string|null
     */
    public function getImage();

    /**
     * Get country
     *
     * @return string|null
     */
    public function getCountry();

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Get address
     *
     * @return string|null
     */
    public function getAddress();

    /**
     * Get work schedule
     *
     * @return string|null
     */
    public function getWorkSchedule();

    /**
     * Get longitude
     *
     * @return float|null
     */
    public function getLongitude();

    /**
     * Get latitude
     *
     * @return float|null
     */
    public function getLatitude();

    /**
     * Get url key
     *
     * @return string
     */
    public function getUrlKey();

    /**
     * Get creation time
     *
     * @return string|null
     */
    public function getCreationTime();

    /**
     * Get update time
     *
     * @return string|null
     */
    public function getUpdateTime();


    /**
     * Set ID
     *
     * @param int $id
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setId($id);

    /**
     * Set store name
     *
     * @param string $name
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setStoreName($name);

    /**
     * Set description
     *
     * @param string $description
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setDescription($description);

    /**
     * Set image
     *
     * @param string $image
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setImage($image);

    /**
     * Set country
     *
     * @param string $country
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setCountry($country);

    /**
     * Set city
     *
     * @param string $city
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setCity($city);

    /**
     * Set address
     *
     * @param string $address
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setAddress($address);

    /**
     * Set work schedule
     *
     * @param string $workSchedule
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setWorkSchedule($workSchedule);

    /**
     * Set longitude
     *
     * @param float $longitude
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setLongitude($longitude);

    /**
     * Set latitude
     *
     * @param float $latitude
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setLatitude($latitude);

    /**
     * Set store url key
     *
     * @param string $urlKey
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setUrlKey($urlKey);

    /**
     * Set creation time
     *
     * @param string $time
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setCreationTime($time);

    /**
     * Set update time
     *
     * @param string $time
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface
     */
    public function setUpdateTime($time);
}
