<?php

namespace Practice\StoreLocator\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface StoreLocatorSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get stores list.
     *
     * @return \Practice\StoreLocator\Api\Data\StoreLocatorInterface[]
     */
    public function getItems();

    /**
     * Set stores list.
     *
     * @param \Practice\StoreLocator\Api\Data\StoreLocatorInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
