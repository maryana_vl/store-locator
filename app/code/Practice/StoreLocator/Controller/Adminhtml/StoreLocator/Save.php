<?php

namespace Practice\StoreLocator\Controller\Adminhtml\StoreLocator;

use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ObjectManager;
use Practice\StoreLocator\Api\StoreLocatorRepositoryInterface;
use Practice\StoreLocator\Model\StoreLocator;
use Practice\StoreLocator\Model\StoreLocatorFactory;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;

class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Practice_StoreLocator::edit';

    /**
     * @var StoreLocatorFactory
     */
    private $storeFactory;
    /**
     * @var StoreLocatorRepositoryInterface
     */
    private $storeLocatorRepository;
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * Index constructor.
     * @param Context $context
     * @param StoreLocatorRepositoryInterface $storeLocatorRepository
     * @param StoreLocatorFactory $storeFactory
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        StoreLocatorRepositoryInterface $storeLocatorRepository,
        StoreLocatorFactory $storeFactory,
        DataPersistorInterface $dataPersistor
    ) {
        parent::__construct($context);
        $this->dataPersistor = $dataPersistor;
        $this->storeFactory = $storeFactory ?: ObjectManager::getInstance()->get(\Practice\StoreLocator\Model\StoreLocatorFactory::class);
        $this->storeLocatorRepository = $storeLocatorRepository ?: ObjectManager::getInstance()->get(\Practice\StoreLocator\Api\StoreLocatorRepositoryInterface::class);
    }

    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();

        $data = $this->getRequest()->getPostValue();

        if ($data) {
            if (empty($data['store_id'])) {
                $data['store_id'] = null;
            }

            /** @var StoreLocator $store */
            $store = $this->storeFactory->create();

            $id = $this->getRequest()->getParam('store_id');
            if ($id) {
                try {
                    $store = $this->storeLocatorRepository->getById($id);
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This store no longer exists.'));
                    return $redirect->setPath('*/*/');
                }
            }

            if (isset($data['image']) && is_array($data['image'])) {
                $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $storeManager = $objectManager->get('Magento\Store\Model\StoreManagerInterface');
                $currentStore = $storeManager->getStore();
                $media_url = $currentStore->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
                $data['image'] = $data['image'][0]['name'];
            } else {
                $data['image'] = null;
            }

            $store->setData($data);

            try {
                $this->storeLocatorRepository->save($store);
                $this->messageManager->addSuccessMessage(__('You saved the store.'));
                return $redirect->setPath("*/*/edit", ['store_id' => $store->getId()]);
            } catch (LocalizedException $e) {
                $this->messageManager->addExceptionMessage($e->getPrevious() ?: $e);
            } catch (\Throwable $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the page.'));
            }

            $this->dataPersistor->set('store_locator', $data);

            return $redirect->setPath("*/*/edit", ['store_id' => $this->getRequest()->getParam('store_id')]);
        }
        return $redirect->setPath('*/*/');
    }
}
