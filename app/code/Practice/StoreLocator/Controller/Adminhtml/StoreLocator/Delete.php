<?php

namespace Practice\StoreLocator\Controller\Adminhtml\StoreLocator;

use Magento\Backend\App\Action;
use Practice\StoreLocator\Api\StoreLocatorRepositoryInterface;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Practice_StoreLocator::view';


    /**
     * @var StoreLocatorRepositoryInterface
     */
    private $storeLocatorRepository;

    /**
     * Delete constructor.
     * @param Action\Context $context
     * @param StoreLocatorRepositoryInterface $storeLocatorRepository
     */
    public function __construct(
        Action\Context $context,
        StoreLocatorRepositoryInterface $storeLocatorRepository
    ) {
        parent::__construct($context);
        $this->storeLocatorRepository = $storeLocatorRepository;
    }

    public function execute()
    {
        $redirect = $this->resultRedirectFactory->create();

        try {
            $storeId = $this->getRequest()->getParam("store_id");
            $this->storeLocatorRepository->deleteById((int)$storeId);
            $this->messageManager->addSuccessMessage("Store was deleted");
        } catch (\Exception $ex) {
            $this->messageManager->addErrorMessage("Error on deleting store");
        }

        return $redirect->setPath("*/*/");
    }
}
