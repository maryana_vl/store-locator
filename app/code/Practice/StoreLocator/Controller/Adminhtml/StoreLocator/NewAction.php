<?php

namespace Practice\StoreLocator\Controller\Adminhtml\StoreLocator;

use Magento\Backend\App\Action\Context;

class NewAction extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Practice_StoreLocator::edit';

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $forwardFactory;


    /**
     * Index constructor.
     * @param Context $context
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $forwardFactory
     */
    public function __construct(
        Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $forwardFactory
    ) {
        parent::__construct($context);
        $this->forwardFactory = $forwardFactory;
    }

    public function execute()
    {
        $resultForward = $this->forwardFactory->create();

        return  $resultForward->forward('edit');
    }
}
