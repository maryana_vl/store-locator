<?php

namespace Practice\StoreLocator\Controller\Adminhtml\StoreLocator;

use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Practice\StoreLocator\Api\StoreLocatorRepositoryInterface;
use Practice\StoreLocator\Model\StoreLocatorFactory;
use Magento\Framework\View\Result\PageFactory;

class Edit extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Practice_StoreLocator::edit';
    /**
     * @var StoreLocatorFactory
     */
    protected $storeLocatorFactory;
    /**
     * @var Registry
     */
    protected $registry;
    /**
     * @var StoreLocatorRepositoryInterface
     */
    protected $storeLocatorRepository;
    /**
     * @var PageFactory
     */
    protected $pageFactory;

    /**
     * Edit constructor.
     * @param Action\Context $context
     * @param Registry $registry
     * @param StoreLocatorRepositoryInterface $storeLocatorRepository
     * @param StoreLocatorFactory $storeLocatorFactory
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context,
        Registry $registry,
        StoreLocatorRepositoryInterface $storeLocatorRepository,
        StoreLocatorFactory $storeLocatorFactory,
        PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->storeLocatorFactory = $storeLocatorFactory;
        $this->registry = $registry;
        $this->storeLocatorRepository = $storeLocatorRepository;
        $this->pageFactory = $pageFactory;
    }

    public function execute()
    {
        $id = $this->getRequest()->getParam('store_id');

        if ($id) {
            $store= $this->storeLocatorRepository->getById($id);
            if (!$store->getId()) {
                $this->messageManager->addErrorMessage(__('This store no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            $store = $this->storeLocatorFactory->create();
        }

        $this->registry->register('store_locator', $store);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->pageFactory->create();
        $resultPage->setActiveMenu('Practice_StoreLocator::all_stores')
            ->addBreadcrumb(__('StoreLocator'), __('StoreLocator'));
        $resultPage->getConfig()->getTitle()->prepend(__('Stores'));
        $resultPage->getConfig()->getTitle()->prepend($store->getId() ? $store->getStoreName() : __('New Store'));

        return $resultPage;
    }
}
