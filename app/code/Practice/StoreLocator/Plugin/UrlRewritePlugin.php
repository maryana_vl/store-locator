<?php

namespace Practice\StoreLocator\Plugin;

use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollection;
use Magento\UrlRewrite\Model\ResourceModel\UrlRewriteCollectionFactory;
use Magento\UrlRewrite\Model\UrlRewriteFactory;
use Practice\StoreLocator\Api\Data\StoreLocatorInterface;
use Practice\StoreLocator\Model\StoreLocatorRepository;

class UrlRewritePlugin
{
    const TARGET_PATH = 'storelocator/store/view/id/';
    const REQUEST_PATH = 'stores/';
    /**
     * @var UrlRewriteFactory
     */
    private $urlRewriteFactory;
    /**
     * @var UrlRewriteCollection
     */
    private $urlRewriteCollection;
    /**
     * @var UrlRewriteCollectionFactory
     */
    private $urlRewriteCollectionFactory;

    /**
     * UrlRewritePlugin constructor.
     * @param UrlRewriteFactory $urlRewriteFactory
     * @param UrlRewriteCollection $urlRewriteCollection
     * @param UrlRewriteCollectionFactory $urlRewriteCollectionFactory
     */
    public function __construct(
        UrlRewriteFactory $urlRewriteFactory,
        UrlRewriteCollection $urlRewriteCollection,
        UrlRewriteCollectionFactory $urlRewriteCollectionFactory
    ) {
        $this->urlRewriteFactory = $urlRewriteFactory;
        $this->urlRewriteCollection = $urlRewriteCollection;
        $this->urlRewriteCollectionFactory = $urlRewriteCollectionFactory;
    }

    public function afterSave(StoreLocatorRepository $subject, StoreLocatorInterface $store)
    {
        /* check if url_rewrite already exists */
        $collection = $this->urlRewriteCollectionFactory->create();
        $collection->addFieldToFilter('target_path', ['eq' => self::TARGET_PATH . $store->getId()]);

        if ($collection->count() == 0) {
            $urlRewriteModel = $this->urlRewriteFactory->create();
            /* set current store id */
            $urlRewriteModel->setStoreId(1);
            /* this url is not created by system so set as 0 */
            $urlRewriteModel->setIsSystem(0);
            /* unique identifier - set random unique value to id path */
            $urlRewriteModel->setIdPath(rand(1, 100000));
            /* set actual url path to target path field */
            $urlRewriteModel->setTargetPath(self::TARGET_PATH . $store->getId());
            /* set requested path which you want to create */
            $urlRewriteModel->setRequestPath(self::REQUEST_PATH . $store->getUrlKey() . '.html');
            /* set current store id */
            $urlRewriteModel->save();
        } elseif ($collection->count() == 1) {
            $requestPath = self::REQUEST_PATH . $store->getUrlKey() . '.html';
            foreach ($collection as $object) {
                if (strcmp($object->getRequestPath(), $requestPath) !== 0) {
                    $object->setRequestPath($requestPath);
                    $object->save();
                }
            }
        }
        return [$store];
    }
}
