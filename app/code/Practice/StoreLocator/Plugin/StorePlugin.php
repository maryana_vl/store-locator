<?php

namespace Practice\StoreLocator\Plugin;

use Practice\StoreLocator\Model\StoreLocator;

class StorePlugin
{
    public function afterGetDescription(StoreLocator $subject, $result)
    {
        if ($result == null) {
            return "-";
        } else {
            return $result;
        }
    }

    public function afterGetWorkSchedule(StoreLocator $subject, $result)
    {
        if (empty($result)) {
            return "-";
        } else {
            return $result;
        }
    }
}
