<?php

namespace Practice\StoreLocator\Observer;

use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Message\ManagerInterface;
use Practice\StoreLocator\Helper\LatLongCoordinates;
use Practice\StoreLocator\Helper\UrlKey;


class StorelocatorSaveBeforeObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var LatLongCoordinates
     */
    private $latLongHelper;
    /**
     * @var ManagerInterface
     */
    private $messageManager;
    /**
     * @var UrlKey
     */
    private $urlKeyHelper;

    /**
     * StoreSaveBeforeObserver constructor.
     * @param LatLongCoordinates $latLongHelper
     * @param UrlKey $urlKeyHelper
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        LatLongCoordinates $latLongHelper,
        UrlKey $urlKeyHelper,
        ManagerInterface $messageManager
    ) {
        $this->latLongHelper = $latLongHelper;
        $this->messageManager = $messageManager;
        $this->urlKeyHelper = $urlKeyHelper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Practice\StoreLocator\Api\Data\StoreLocatorInterface $store */
        $store = $observer->getStorelocator();

        if ($store->getLatitude() == null || $store->getLongitude() == null) {
            $address = $store->getCountry() . ',' . $store->getCity() . ',' . $store->getAddress();
            try {
                $coordinates = $this->latLongHelper->getLatLong($address);
                $store->setLatitude($coordinates['latitude']);
                $store->setLongitude($coordinates['longitude']);
            } catch (CouldNotSaveException $exception) {
                $this->messageManager->addNoticeMessage($exception->getMessage());
            }

        }

        if (strlen($store->getUrlKey()) == 0) {
            try {
                $urlKey = $this->urlKeyHelper->getStoreUrlKey($store->getStoreName());
                $store->setUrlKey($urlKey);
            } catch (CouldNotSaveException $exception) {
                $this->messageManager->addNoticeMessage($exception->getMessage());
            }
        }
    }
}
